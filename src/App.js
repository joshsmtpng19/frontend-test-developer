import logo from './logo.svg';
import './App.css';
import { Route, Routes } from "react-router-dom";
import LandingPage from './pages/landing-page/LandingPage';
import Login from './pages/login/Login';
import Home from './pages/home/Home';
import Detail from './components/detail/Detail';
import Profile from './components/profile/Profile';

function App() {
  return (
    <Routes>
      <Route path="/" element={<LandingPage />} />
      <Route path="/login" element={<Login />} />
      <Route path="/home" element={<Home />} />
      <Route path="/status/:id" element={<Detail />} />
      <Route path="/profile/:name" element={<Profile />} />
    </Routes>
  );
}

export default App;
