import React, { useState } from 'react';
import './profile.css';
import Header from '../header/Header';
import {useNavigate} from 'react-router-dom';
import WestIcon from '@mui/icons-material/West';

export default function Profile() {
  const [profilesText, setProfileTexts] = useState({
    'username': localStorage.getItem("username"),
    'email': localStorage.getItem("email"),
    'address': localStorage.getItem("address"),
    'phone': localStorage.getItem("phone") 
  })
  let navigate = useNavigate(); 
  return (
    <div className='profile-container'>
        <Header page={'home'}/>
        <div className='content'>
        <div dir="ltr" onClick={() => { navigate(`/home`, { replace: true }) }} className="back-icon-container"><WestIcon className='icon-back'/></div>
        </div>
        <div className='profile-text-container'>
            {/* left-side */}
            <div className='left-side'>
                <ul>
                    <li>Username</li>
                    <li>Email</li>
                    <li>Address</li>
                    <li>Phone</li>
                </ul>
            </div>
            <div className='mid-side'>
                <ul>
                    <li>:</li>
                    <li>:</li>
                    <li>:</li>
                    <li>:</li>
                </ul>
            </div>
            {/* right-side */}
            <div className='right-side'>
                <ul>
                    <li>{profilesText.username}</li>
                    <li>{profilesText.email}</li>
                    <li>{profilesText.address}</li>
                    <li>{profilesText.phone}</li>
                </ul>
            </div>
        </div>
    </div>
  )
}
