import React from 'react';
import Header from '../../components/header/Header';
import './home.css';
import Posts from '../../components/posts/Posts';

export default function Home(props) {
  return (
    <div className='home-container'>
        <Header page={'home'}/>
        <Posts/>
    </div>
  )
}
